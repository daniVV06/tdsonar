package pizzas;

public class Ingredient {

	private String nomIng;
	private boolean vegetarien;
	public String getNom() {
		return nomIng;
	}
	public boolean isVegetarien() {
		return vegetarien;
	}
	public Ingredient(String nom, boolean vegetarien) {
		this.nomIng = nom;
		this.vegetarien = vegetarien;
	}

	public boolean equals(Ingredient i) {
		return nomIng.equals(i.getNom());
	}
	
}
